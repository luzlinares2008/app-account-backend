//Requires
var express = require('express');
const cors = require('cors');
//Init variables
var express_var = express();
const bodyParser = require('body-parser');

var transactionService = require('../services/transaction_service');

//middleware para habilitar el CORS
express_var.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,token");
    res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
    next();
});

//middleware: se ejecuta antes de la petición
express_var.use(bodyParser.urlencoded({extend: false}));
express_var.use(bodyParser.json());

express_var.use(cors());
express_var.options('*', cors());

//:::::::::::::::::::::::::::::::::::::::::::::::
//::::::Métodos para User::::::
//:::::::::::::::::::::::::::::::::::::::::::::::

// Lista todos las skills
express_var.get('/transaction', (req, res) => {
    console.log('Atendiendo GET /transaction');
    transactionService.listar(req, res).then(response => {
        res.json(response);
    }).catch(error => {
        res.status(500).send(error)
    });
});

// Query
express_var.get('/transaction/query', (req, res) => {
    console.log('Atendiendo GET /transaction');
    transactionService.query(req, res).then(response => {
        res.json(response);
    }).catch(error => {
        res.status(500).send(error)
    });
});

express_var.get('/transaction/query-monthly-tx', (req, res) => {
    console.log('Atendiendo GET /transaction');
    transactionService.queryMonthlyTx(req, res).then(response => {
        res.json(response);
    }).catch(error => {
        res.status(500).send(error)
    });
});

express_var.get('/transaction/query-yearly-tx', (req, res) => {
    console.log('Atendiendo GET /transaction');
    transactionService.queryYearlyTx(req, res).then(response => {
        res.json(response);
    }).catch(error => {
        res.status(500).send(error)
    });
});

//Lista skill por Id
express_var.post('/transaction/:_id', (req, res) => {
    console.log('Atendiendo GET /transaction/:_id');
    var _id = req.body._id;
    transactionService.listarPorId(_id, req, res).then(response => {
        res.json(response);
    }).catch(error => {
        res.status(500).send(error)
    });
});

//Crea nueva skill
express_var.post('/transaction', (req, res) => {
    console.log('Atendiendo POST /transaction');
    var body = req.body;
    transactionService.crear(body, req, res).then(response => {
        res.json(response);
    }).catch(error => {
        res.status(500).send(error)
    });

});

//Actualiza user
express_var.put('/transaction/:_id', (req, res) => {
    console.log('Atendiendo PUT /transaction/:_id');
    var body = req.body;
    transactionService.actualizar(body, req, res).then(response => {
        res.json(response);
    }).catch(error => {
        res.status(500).send(error)
    });

});

//Elimina(logico) user
express_var.delete('/transaction/:_id', (req, res) => {
    console.log('Atendiendo DELETE /transaction/:_id');
    var body = req.body;
    transactionService.eliminar(body, req, res).then(response => {
        res.json(response);
    }).catch(error => {
        res.status(500).send(error)
    });

});


module.exports = express_var;
