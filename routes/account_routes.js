//Requires
var express = require('express');
const cors = require('cors');
//Init variables
var express_var = express();
const bodyParser = require('body-parser');

var accountService = require('../services/account_service');

//middleware para habilitar el CORS
express_var.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,token");
    res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
    next();
});

//middleware: se ejecuta antes de la petición
express_var.use(bodyParser.urlencoded({extend: false}));
express_var.use(bodyParser.json());

express_var.use(cors());
express_var.options('*', cors());

//:::::::::::::::::::::::::::::::::::::::::::::::
//::::::Métodos para User::::::
//:::::::::::::::::::::::::::::::::::::::::::::::

// Lista todos las skills
express_var.get('/account', (req, res) => {
    console.log('Atendiendo GET /account');
    accountService.listar(req, res).then(response => {
        res.json(response);
    }).catch(error => {
        res.status(500).send(error)
    });
});

//Lista skill por Id
express_var.post('/account/:_id', (req, res) => {
    console.log('Atendiendo GET /account/:_id');
    var _id = req.body._id;
    accountService.listarPorId(_id, req, res).then(response => {
        res.json(response);
    }).catch(error => {
        res.status(500).send(error)
    });
});

//Crea nueva skill
express_var.post('/account', (req, res) => {
    console.log('Atendiendo POST /account');
    var body = req.body;

    accountService.crear(body, req, res).then(response => {
        res.json(response);
    }).catch(error => {
        res.status(500).send(error)
    });

});

//Actualiza user
express_var.put('/account/:_id', (req, res) => {
    console.log('Atendiendo PUT /account/:_id');
    var body = req.body;
    accountService.actualizar(body, req, res).then(response => {
        res.json(response);
    }).catch(error => {
        res.status(500).send(error)
    });

});

//Elimina(logico) user
express_var.delete('/account/:_id', (req, res) => {
    console.log('Atendiendo DELETE /account/:_id');
    var body = req.body;
    accountService.eliminar(body, req, res).then(response => {
        res.json(response);
    }).catch(error => {
        res.status(500).send(error)
    });

});


module.exports = express_var;
