//Requires
var express = require('express');
const cors = require('cors');
//Init variables
var express_var = express();
const bodyParser = require('body-parser');

var userService = require('../services/user_service');

//middleware para habilitar el CORS
express_var.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,token");
    res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
    next();
});

//middleware: se ejecuta antes de la petición
express_var.use(bodyParser.urlencoded({extend: false}));
express_var.use(bodyParser.json());

express_var.use(cors());
express_var.options('*', cors());

//:::::::::::::::::::::::::::::::::::::::::::::::
//::::::Métodos para User::::::
//:::::::::::::::::::::::::::::::::::::::::::::::

// Lista todos las skills
express_var.get('/user', (req, res) => {
    console.log('Atendiendo GET /user');
    userService.listar(req, res).then(response => {
        res.json(response);
    }).catch(error => {
        res.status(500).send(error)
    });
});

//Lista skill por Id
express_var.get('/user/:_id', (req, res) => {
    console.log('Atendiendo GET /user/:_id');
    var _id = req.params._id;
    userService.listarPorId(_id, req, res).then(response => {
        res.json(response);
    }).catch(error => {
        res.status(500).send(error)
    });
});

//Crea nueva skill
express_var.post('/user', (req, res) => {
    console.log('Atendiendo POST /user');
    var body = req.body;

    userService.crear(body, req, res).then(response => {
        console.log("response",response);
        res.json(response);
    }).catch(error => {
        res.status(500).send(error)
    });

});

//Actualiza user
express_var.put('/user/:_id', (req, res) => {
    console.log('Atendiendo PUT /user/:_id');
    var body = req.body;
    userService.actualizar(body, req, res).then(response => {
        res.json(response);
    }).catch(error => {
        res.status(500).send(error)
    });

});

//Elimina(logico) user
express_var.delete('/user/:_id', (req, res) => {
    console.log('Atendiendo DELETE /user/:_id');
    var body = req.body;
    userService.eliminar(body, req, res).then(response => {
        res.json(response);
    }).catch(error => {
        res.status(500).send(error)
    });

});

// Login
express_var.post('/login', (req, res) => {
    var body = req.body;
    body.logged = true;
    userService.buscarPorEmail(body, req, res).then(usuarioDB=>{
        if(usuarioDB.length<1){
            return res.status(400).json({
                ok:false,
                mensaje:'No existe usuario con el registro ingresado!'
            });
        }

        if(body.password !== usuarioDB[0].password){
            return res.status(400).json({
                ok:false,
                mensaje:'Credenciales incorrectas'
            });
        }

        //Quita la contraseña para no enviarla al browser.
        usuarioDB[0].password = '*****';

       //Si logeo es exitoso devuelve el usuario
        return res.status(200).json({
            ok:true,
            usuario:usuarioDB,
            mensaje:'Logeo exitoso!'
        });

    }).catch( error => {
        res.status( 500 ).send( error )
    });

});

module.exports = express_var;
