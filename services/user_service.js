var User = require('../models/user_model');

var accountService = require('../services/account_service');
const { mongoose } = require('../configurations/mongoose_config');
const EmailAlreadyExists = require('../exceptions/errors')

exports.listar = function (req, res) {

    var limit = parseInt(req.query.limit);
    var page = (parseInt(req.query.page) <= 0 ? parseInt(1) : parseInt(req.query.page));
    var query = {};
    if(req.query.logged === "true"){
        query.logged = true;
    }else{
        query.logged = false;
    }

    var aggregate = User.aggregate().match(query);
    return User.aggregatePaginate(aggregate, {page: page, limit: limit}).then(
        models => models
    ).catch(error => {
        res.status(500).send(error);
    });
}

exports.listarPorId = function (_id, req, res) {
    console.log("_id",_id);
    return User.find({"idUsuario": _id}).then(
        models => models
    ).catch(error => {
        res.status(500).send(error);
    });
}

exports.crear = function (body, req, res) {

    var model = new User({
        logged: true,
        first_name: body.first_name,
        last_name: body.last_name,
        email: body.email,
        password: body.password,
        dni: body.dni,
        phone: body.phone,
        create_at: new Date(),
        role: "user"

    });

    let modelResponse;

    return this.buscarPorEmail({email: body.email},req,res).then(users=>{
        if(users.length>0){
            throw new EmailAlreadyExists("El email ya esta registrado");
        }
        return model.save();
    }).then(modelCreated => {

        modelResponse = modelCreated;

        var bodyAccount = {
            currency: "Soles",
            userID: modelCreated.idUsuario
        }

        return accountService.crear(bodyAccount,req,res);

    }).then(accountCreated => {
            var model = modelResponse._doc;
            model.account = accountCreated._doc;
            return model;
        }).catch( ex => {
        console.log("ex",ex);
        res.status(400).send({error: ex.name,message: ex.message});
    });

}

exports.actualizar = function (body, req, res) {
    var query = {_id: body._id};
    console.log(body);

    return this.buscarPorEmail({email: body.email, idUsuario : body.idUsuario},req,res)
        .then(users=>{
        if(users.length>0){
            throw new EmailAlreadyExists("El email ya esta registrado");
        }
        return User.findOneAndUpdate(query, body, {useFindAndModify: false, new: true});
    }).then(modelUpdated => {
                return modelUpdated;
    }).catch( ex => {
        console.log("ex",ex);
        res.status(400).send({error: ex.name,message: ex.message});
    });

    return User.findOneAndUpdate(query, body, {useFindAndModify: false, new: true}).then(
        modelUpdated => modelUpdated
    ).catch(error => {
        res.status(500).send(error);
    });
}

exports.eliminar = function (body, req, res) {
    var query = {_id: body._id};
    body = {_id: body._id, logged: false};
    return User.findOneAndUpdate(query, body, {useFindAndModify: false, new: true}).then(
        modelUpdated => modelUpdated
    ).catch(error => {
        res.status(500).send(error);
    });
}

exports.buscarPorEmail = function (body,req,res){
    var query = {email: body.email};
    if(body.logged){
        query.logged = true;
    }
    if(body.idUsuario){
        query.idUsuario = {$ne:body.idUsuario};
    }
    return User.find(query).then(
        usuarios => usuarios
    ).catch(error => {
        res.status(500).send(error);
    });
}
