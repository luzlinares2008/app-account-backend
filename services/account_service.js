var Account = require('../models/account_model');

const {mongoose} = require('../configurations/mongoose_config');

exports.listar = function (req, res) {

    // var limit = parseInt(req.query.limit);
    // var page = (parseInt(req.query.page) <= 0 ? parseInt(1) : parseInt(req.query.page));
    var query = {active: true};
    if(req.query.userID){
        query.userID=parseInt(req.query.userID);
    }

    // var aggregate = Account.aggregate().match(query);
    // return Account.aggregatePaginate(aggregate, {page: page, limit: limit}).then(
    //     models => models
    // ).catch(error => {
    //     res.status(500).send(error);
    // });
    return Account.find(query).then(
        models => models
    ).catch(error => {
        res.status(500).send(error);
    });
}

exports.listarPorId = function (_id, req, res) {
    return Account.find({"_id": _id, active: true}).then(
        models => models
    ).catch(error => {
        res.status(500).send(error);
    });
}

exports.crear = async function (body, req, res) {

    var dateInit = new Date();
    var dateEnd = new Date();
    dateEnd.setFullYear(dateEnd.getFullYear() + 1);

    var ibam = await generateIbam();

    var model = new Account({
        active: true,
        IBAM: ibam,
        balance: 0,
        userID: body.userID,
        date_init: dateInit,
        date_end: dateEnd,
        currency: body.currency
    });

    return model.save().then(
        modelCreated => modelCreated
    ).catch(error => {
        res.status(500).send(error);
    });
}

exports.actualizar = function (body, req, res) {
    var query = {_id: body._id}
    return Account.findOneAndUpdate(query, body, {useFindAndModify: false, new: true}).then(
        modelUpdated => modelUpdated
    ).catch(error => {
        res.status(500).send(error);
    });
}

exports.eliminar = function (body, req, res) {
    var query = {_id: body._id};
    body = {_id: body._id, active: false};
    return Account.findOneAndUpdate(query, body, {useFindAndModify: false, new: true}).then(
        modelUpdated => modelUpdated
    ).catch(error => {
        res.status(500).send(error);
    });
}

exports.actualizarBalance = function (userID,accountID, balance) {
    var query = {userID:userID,accountID: accountID}
    return Account.findOneAndUpdate(query, {balance: balance}, {useFindAndModify: false, new: true}).then(
        modelUpdated => modelUpdated
    ).catch(error => {
        res.status(500).send(error);
    });
}

 async function generateIbam() {
     var isNew = false
     var accountNumber;
     do {
         var accountBase = "001177991202";
         var final = accountBase + Math.floor(Math.random() * 100000000);
         const res = await Account.find({"IBAM": final});
         if (res.length === 0) {
             accountNumber = final;
             isNew = true;
         }
     } while (!isNew);
     return accountNumber;
 }
