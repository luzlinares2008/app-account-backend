var Transaction = require('../models/transacction_model');
const NotEnoughMoney = require('../exceptions/errors')

const {mongoose} = require('../configurations/mongoose_config');
var accountService = require('../services/account_service');

exports.listar = function (req, res) {

    // var limit = parseInt(req.query.limit);
    // var page = (parseInt(req.query.page) <= 0 ? parseInt(1) : parseInt(req.query.page));
    var query = {active: true};

    if(req.query.userID){
        query.userID = parseInt(req.query.userID);
    }
    if(req.query.accountID){
        query.accountID = parseInt(req.query.accountID);
    }

    // var aggregate = Transaction.aggregate().match(query);
    // return Transaction.aggregatePaginate(aggregate, {page: page, limit: limit}).then(
    //     models => models
    // ).catch(error => {
    //     res.status(500).send(error);
    // });

    console.log("query",query);

    return Transaction.find(query).sort({create_at:'desc'}).then(
        models => {
            console.log("models",models);
            return models;
        }
    ).catch(error => {
        res.status(500).send(error);
    });
}

exports.listarPorId = function (_id, req, res) {
    return Transaction.find({"_id": _id, active: true}).then(
        models => models
    ).catch(error => {
        res.status(500).send(error);
    });
}

exports.crear = function (body, req, res) {

    var totalDepositos = 0;
    var totalRetiros = 0
    var totalBalance = 0;
    var money = parseFloat(body.money);

    var model = new Transaction({
        active: true,
        tipo: body.tipo,
        detalle: body.detalle,
        money: money,
        accountID: body.accountID,
        userID: body.userID,
        create_at : (body.create_at ? body.create_at : new Date())
    });

    return sumByTipo(parseInt(body.userID),body.accountID,"Deposito",-1).then(
        response => {
            if(response.length>0){
                totalDepositos = (response[0].totalMoney ? response[0].totalMoney: 0);
            }else{
                totalDepositos = 0;
            }
            return sumByTipo(parseInt(body.userID),body.accountID,"Retiro",-1)
        }
    ).then(
        response => {
            if(response.length>0){
                totalRetiros = (response[0].totalMoney ? response[0].totalMoney: 0);
            }else{
                totalRetiros = 0;
            }

            totalBalance = totalDepositos-totalRetiros;
            if(body.tipo === 'Deposito'){
                return accountService.actualizarBalance(parseInt(body.userID),body.accountID,(totalBalance+money));
            }else{
                if((totalBalance-money) >= 0){
                    return accountService.actualizarBalance(parseInt(body.userID),body.accountID,(totalBalance-money));
                }else{
                    throw new NotEnoughMoney("Saldo insuficiente");
                }
            }

        }
    ).then(
        response => {
            return model.save();
        }
    ).then(
        modelCreated => modelCreated
    ).catch(ex => {
        console.log("ex",ex);
        res.status(400).send({error: ex.name,message: ex.message});
    });

    // return model.save().then(
    //     modelCreated => modelCreated
    // ).catch(error => {
    //     res.status(500).send(error);
    // });
}

exports.actualizar = function (body, req, res) {
    var query = {_id: body._id}
    return Transaction.findOneAndUpdate(query, body, {useFindAndModify: false, new: true}).then(
        modelUpdated => modelUpdated
    ).catch(error => {
        res.status(500).send(error);
    });
}

exports.eliminar = function (body, req, res) {
    var query = {_id: body._id};
    body = {_id: body._id, active: false};
    return Transaction.findOneAndUpdate(query, body, {useFindAndModify: false, new: true}).then(
        modelUpdated => modelUpdated
    ).catch(error => {
        res.status(500).send(error);
    });
}

exports.query = function (req, res) {

    var query = {active:true};

    var createAtInit = null;
    var createAtEnd = null;

    if(parseInt(req.query.userID)){
        query.userID = parseInt(req.query.userID);
    }

    if(parseInt(req.query.accountID)){
        query.accountID = parseInt(req.query.accountID);
    }

    if(req.query.tipo){
        query.tipo = req.query.tipo;
    }

    if(req.query.create_at_init){
        createAtInit = new Date(req.query.create_at_init+"T00:00:00.0Z");
    }else{
        createAtInit = new Date("1900-01-01T00:00:00.0Z");
    }

    if(req.query.create_at_end){
        createAtEnd = new Date(req.query.create_at_end+"T23:59:00.0Z");
    }else{
        createAtEnd = new Date("3000-01-01T23:59:00.0Z");
    }

    var aggregate = Transaction.aggregate(
        [
            { $match: {create_at: {$gte: createAtInit, $lte: createAtEnd}} },
            { $match: query },
            { $lookup:{ from:"account", localField:"accountID", foreignField:"accountID", as:"account" } },
            { $lookup:{ from:"user", localField:"userID", foreignField:"idUsuario", as:"user" } },
            { $unwind : "$account" },
            { $unwind : "$user" }
        ]
    );
    return aggregate.then(
        models => models
    ).catch(error => {
        res.status(500).send(error);
    });
}

exports.queryMonthlyTx = function (req, res) {

    var query = {active:true};

    var createAtInit = null;
    var createAtEnd = null;

    if(parseInt(req.query.userID)){
        query.userID = parseInt(req.query.userID);
    }

    if(parseInt(req.query.accountID)){
        query.accountID = parseInt(req.query.accountID);
    }

    if(req.query.create_at_init){
        createAtInit = new Date(req.query.create_at_init+"T00:00:00.0Z");
    }else{
        createAtInit = new Date("1900-01-01T00:00:00.0Z");
    }

    if(req.query.create_at_end){
        createAtEnd = new Date(req.query.create_at_end+"T23:59:00.0Z");
    }else{
        createAtEnd = new Date("3000-01-01T23:59:00.0Z");
    }

    var aggregate = Transaction.aggregate(
        [

            { $sort : { userID : -1 } },
            { $match: {create_at: {$gte: createAtInit, $lte: createAtEnd}} },
            { $match: query },
            { $lookup:{ from:"account", localField:"accountID", foreignField:"accountID", as:"account" } },
            { $lookup:{ from:"user", localField:"userID", foreignField:"idUsuario", as:"user" } },
            { $unwind : "$account" },
            { $unwind : "$user" },
            {
                $group:
                    {
                        _id: { month: { $month: "$create_at"}, year: { $year: "$create_at" }, user: "$user",account: "$account" },
                        totalDeposito:   { $sum:   {$cond: {if: {$eq: ['$tipo', 'Deposito']},then: '$money',else: 0}}},
                        totalRetiro:   { $sum:   {$cond: {if: {$eq: ['$tipo', 'Retiro']},then: '$money',else: 0}}},
                        countDeposito:   { $sum:   {$cond: {if: {$eq: ['$tipo', 'Deposito']},then: 1,else: 0}}},
                        countRetiro:   { $sum:   {$cond: {if: {$eq: ['$tipo', 'Retiro']},then: 1,else: 0}}},
                        count: { $sum: 1 }
                    }
            }
        ]
    );
    return aggregate.then(
        models => models
    ).catch(error => {
        res.status(500).send(error);
    });
}

exports.queryYearlyTx = function (req, res) {

    var query = {active:true};

    var createAtInit = null;
    var createAtEnd = null;

    if(parseInt(req.query.userID)){
        query.userID = parseInt(req.query.userID);
    }

    if(parseInt(req.query.accountID)){
        query.accountID = parseInt(req.query.accountID);
    }

    if(req.query.create_at_init){
        createAtInit = new Date(req.query.create_at_init+"T00:00:00.0Z");
    }else{
        createAtInit = new Date("1900-01-01T00:00:00.0Z");
    }

    if(req.query.create_at_end){
        createAtEnd = new Date(req.query.create_at_end+"T23:59:00.0Z");
    }else{
        createAtEnd = new Date("3000-01-01T23:59:00.0Z");
    }

    var aggregate = Transaction.aggregate(
        [

            { $sort : { userID : -1 } },
            { $match: {create_at: {$gte: createAtInit, $lte: createAtEnd}} },
            { $match: query },
            { $lookup:{ from:"account", localField:"accountID", foreignField:"accountID", as:"account" } },
            { $lookup:{ from:"user", localField:"userID", foreignField:"idUsuario", as:"user" } },
            { $unwind : "$account" },
            { $unwind : "$user" },
            {
                $group:
                    {
                        _id: { year: { $year: "$create_at" }, user: "$user",account: "$account" },
                        totalDeposito:   { $sum:   {$cond: {if: {$eq: ['$tipo', 'Deposito']},then: '$money',else: 0}}},
                        totalRetiro:   { $sum:   {$cond: {if: {$eq: ['$tipo', 'Retiro']},then: '$money',else: 0}}},
                        countDeposito:   { $sum:   {$cond: {if: {$eq: ['$tipo', 'Deposito']},then: 1,else: 0}}},
                        countRetiro:   { $sum:   {$cond: {if: {$eq: ['$tipo', 'Retiro']},then: 1,else: 0}}},
                        count: { $sum: 1 }
                    }
            }
        ]
    );
    return aggregate.then(
        models => models
    ).catch(error => {
        res.status(500).send(error);
    });
}

function sumByTipo(userID,accountID,tipoMovimiento,Transactionid){
   var aggregate = Transaction.aggregate(
        // Limit to relevant documents and potentially take advantage of an index
[
    {
        $match: {
            Transactionid: {$ne:Transactionid},
            accountID: accountID,
            userID: userID,
            tipo: tipoMovimiento,
            active: true
        }
    },
    { $group: {
            _id: null,
            totalMoney:   { $sum: "$money" }
        }}
]
    )
    return aggregate;
}
