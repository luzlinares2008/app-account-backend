class NotEnoughMoney extends Error {
    constructor (message) {
        super(message)

        this.name = this.constructor.name
    }
}


class EmailAlreadyExists extends Error {
    constructor (message) {
        super(message)

        this.name = this.constructor.name
    }
}

module.exports = NotEnoughMoney
module.exports = EmailAlreadyExists
