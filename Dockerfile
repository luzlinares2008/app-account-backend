FROM node:12-alpine
MAINTAINER luzlinares@2008@gmail.com
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY . .
RUN npm install
EXPOSE 3000
CMD ["npm", "run", "start"]
