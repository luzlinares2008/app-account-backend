//Requires
require('dotenv').config();
var express = require('express');

//Init variables
var express_var = express();
var userRoutes = require('./routes/user_routes');
var accountRoutes = require('./routes/account_routes');
var transactionRoutes = require('./routes/transaction_routes');

//express_var.use(cors);
//express_var.options('*', cors());

//Rutas
express_var.use('/',userRoutes);
express_var.use('/',accountRoutes);
express_var.use('/',transactionRoutes);

//Listenning requests
var port = process.env.PORT || 3000;
express_var.listen(port,()=>{
    console.log('Express server puerto '+port);
});

module.exports = {
    express_var
}