var mongoose = require('mongoose');
var aggregatePaginate = require('mongoose-aggregate-paginate-v2');
var counters = require('../models/counter_model');

var Schema = mongoose.Schema;

var userSchema = new Schema({
    idUsuario: {type: Number},
    first_name: {type: String},
    last_name: {type: String},
    email: {type: String},
    dni: {type: String},
    phone: {type: String},
    create_at: {type: Date},
    password: {type: String},
    logged: {type: Boolean},
    role: {type: String}
});

userSchema.pre('save', function(next) {
    var doc = this;
    counters.findByIdAndUpdate({_id: 'user'}, {$inc: { sequence_value: 1} }, function(error, counter)   {
        if(error)
            return next(error);
        doc.idUsuario = counter.sequence_value;
        next();
    });
});

userSchema.plugin(aggregatePaginate);
module.exports = mongoose.model('user', userSchema, "user");
