var mongoose = require('mongoose');
var aggregatePaginate  = require('mongoose-aggregate-paginate-v2');
var counters = require('../models/counter_model');

var Schema = mongoose.Schema;

var accountSchema = new Schema({
  accountID:{type: Number},
  IBAM:{type: String},
  balance:{type: Number},
  userID:{type: Number},
  active: {type: Boolean},
  date_init: {type: Date},
  date_end: {type: Date},
  currency: {type: String}
});


accountSchema.pre('save', function(next) {
  var doc = this;
  counters.findByIdAndUpdate({_id: 'account'}, {$inc: { sequence_value: 1} }, function(error, counter)   {
    if(error)
      return next(error);
    doc.accountID = counter.sequence_value;
    next();
  });
});

accountSchema.plugin(aggregatePaginate);
module.exports = mongoose.model('account',accountSchema,"account");
