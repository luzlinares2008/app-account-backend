var mongoose = require('mongoose');
var aggregatePaginate  = require('mongoose-aggregate-paginate-v2');
var counters = require('../models/counter_model');

var Schema = mongoose.Schema;

var transactionSchema = new Schema({
  Transactionid:{type: Number},
  tipo:{type: String},
  detalle:{type: String},
  money:{type: Number},
  accountID:{type: Number},
  userID:{type: Number},
  active: {type: Boolean},
  create_at: {type: Date}
});

transactionSchema.pre('save', function(next) {
  var doc = this;
  counters.findByIdAndUpdate({_id: 'transaction'}, {$inc: { sequence_value: 1} }, function(error, counter)   {
    if(error)
      return next(error);
    doc.Transactionid = counter.sequence_value;
    next();
  });
});

// transactionSchema.plugin(aggregatePaginate);
module.exports = mongoose.model('transaction',transactionSchema,"transaction");
