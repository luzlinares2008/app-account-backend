var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var counterSchema = new Schema({
    _id: {type: String},
    sequence_value: {type: Number}
});
module.exports = mongoose.model('counters', counterSchema, "counters");
